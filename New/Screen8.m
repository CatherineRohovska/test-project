//
//  Screen8.m
//  New
//
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Screen8.h"

@interface Screen8 ()

@end

@implementation Screen8

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:false];
    //create tableview
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
   //
    //create gradient
    CAGradientLayer* theViewGradient = [Tour createLayerBlueGradient];
    theViewGradient.frame = self.tabView.bounds;
    //Add gradient to view
    [self.tableView.layer insertSublayer:theViewGradient atIndex:0];
    // Do any additional setup after loading the view.
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.view addSubview:self.tableView];
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
        
    // Fetch the devices from data store
   
    [self.tabView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DataStorage* dataStorage = [DataStorage sharedStorage];
    return dataStorage.devices.count;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    DataStorage* dataStorage = [DataStorage sharedStorage];
    
    TourEntry *device = [/*self.devices*/ dataStorage.devices objectAtIndex:indexPath.row];

    [cell.textLabel setText: device.name];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter]; //adding string in the center of the cell
   
     NSString *workSpacePath=[[Tour applicationDocumentsDirectory] stringByAppendingPathComponent:device.picpath];
     
    //////
    //set image to cell
    cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfFile:workSpacePath]];
    
    
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataManager* dataManager = [DataManager sharedManager];
    DataStorage* dataStorage = [DataStorage sharedStorage];
    
           if (editingStyle == UITableViewCellEditingStyleDelete) {
            //remove files from disk
            TourEntry *device = [dataStorage.devices objectAtIndex:indexPath.row];
            NSString *workSpacePath=[Tour applicationDocumentsDirectory] ;
            [[NSFileManager defaultManager] removeItemAtPath:[workSpacePath stringByAppendingPathComponent:device.picpath ]error:NULL];
            [[NSFileManager defaultManager] removeItemAtPath:[workSpacePath stringByAppendingPathComponent:device.audiopath ]error:NULL];
        // Remove device from table view and storage
               NSLog(@"%lu", (unsigned long)dataStorage.devices.count);
       [dataManager deleteObj: [dataStorage.devices objectAtIndex:indexPath.row]]; //from storage
                NSLog(@"%lu", (unsigned long)dataStorage.devices.count);
        [ dataStorage.devices removeObjectAtIndex:indexPath.row]; //from prorgam storage
        [self.tabView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
    }
}
//tap to select row and open the galaery on selected index
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Screen9 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen9"];
    ctrl.bookmark = (int)indexPath.item; //adding mark to scroll CollectionView
   //  NSLog(@"%ld",(long)ctrl.bookmark);
    [self.navigationController pushViewController:ctrl animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) dealloc{
    NSLog(@"dealloc list detected");
}
@end
