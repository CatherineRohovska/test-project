//
//  TourEntry.m
//  New
//
//  Created by User on 9/28/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "TourEntry.h"

@implementation TourEntry
@dynamic name;
@dynamic picpath;
@dynamic audiopath;
@dynamic latitude;
@dynamic longitude;
@end
