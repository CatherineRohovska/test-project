//
//  Screen6.m
//  New
//
//  Created by User on 9/22/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Screen6.h"

@interface Screen6 (){
AVAudioRecorder *recorder;
    AVAudioPlayer *player;
//timer
    NSTimer *timer;
    int seconds;
    bool success;}

@end

@implementation Screen6
@synthesize recordPauseButton = _recordPauseButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //set background
    //create gradient
    CAGradientLayer* theViewGradient = [Tour createLayerBlueGradient];
    theViewGradient.frame = self.view.bounds;
    //Add gradient to view
    [self.view.layer insertSublayer:theViewGradient atIndex:0];
    UIButton *anotherButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    anotherButton.frame = CGRectMake(0, 0, 80, 30);
    [anotherButton setTitle:@"Next" forState:UIControlStateNormal];
    [anotherButton addTarget:self action:@selector(toSave) forControlEvents:UIControlEventTouchUpInside];
    [anotherButton setBackgroundImage:[UIImage imageNamed:@"button_background.png"] forState:UIControlStateNormal];
    UIBarButtonItem* nextButton = [[UIBarButtonItem alloc] initWithCustomView:anotherButton];
    self.navigationItem.rightBarButtonItem = nextButton;

    //add disable next button
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [_playButton setEnabled:NO];
    //success = NO;
}
-(void) viewDidAppear:(BOOL)animated{
    success = NO;
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               [[self.name stringByAppendingString:[Tour timeStamp]] stringByAppendingString: @".m4a"],
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    //add core data with unique timestamp
    DataManager* dataManager = [DataManager sharedManager];
    dataManager.device.audiopath =[[self.name stringByAppendingString:[Tour timeStamp]] stringByAppendingString: @".m4a"];
   // [dataManager addAudioPath: [[self.name stringByAppendingString:[Tour timeStamp]] stringByAppendingString: @".m4a"]];
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc]  initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    
    //initiate timer label
    [self.timerLabel setText:@"0:00"];
    seconds = 0;
    [self.levelMeter setProgress:0 animated:NO];
}

//on timer tick
- (void)updateTimer:(NSTimer *)theTimer {
    if (seconds>=0) {
        if(seconds==0){[self.timerLabel setText:@"0:00"];}
        seconds++;
        int min = seconds/60;
        int sec = seconds%60;
        [self.timerLabel setText:[NSString stringWithFormat:@"%01d:%02d",min,sec]];
        //
        //update level meters of sound
        [recorder updateMeters];
        [self.levelMeter setProgress:((160+[recorder averagePowerForChannel:0])/160) animated:YES];
        //if audio recording more then 5 minutes
        if(min>5){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Record stopped"
                                                            message: @"Sorry, it has limit on 5 minutes"
                                                           delegate: nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [timer invalidate];
            [recorder stop];
        }
    } else {
        [timer invalidate];
        NSLog(@"Timer end");
    }
   
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [ super viewWillDisappear:animated];
    //when back button taped
    [timer invalidate];
    if (player.playing) {
        [player stop];
    }
    if (recorder.recording){
        [recorder stop];
        [recorder deleteRecording];//nothing to save here
    }
    else{
  if (!success) //if returns back not taping Next
  {
      [recorder deleteRecording];
  }
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)recordPauseTaped:(id)sender {
    // Stop the audio player before recording
    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        
        //initiate timer
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0f  target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
        //butons disabled
        [_recordPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        [_playButton setEnabled:NO];
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    } else {
        
        // Pause recording
        [recorder pause];
        [timer invalidate];
        NSLog(@"Timer paused");
         [_recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
       
    }
    
    
}

- (IBAction)stopTaped:(id)sender {
    if (recorder.recording){
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    //timer stop
    [timer invalidate];
    NSLog(@"Timer stopped");
    seconds = 0;
    [_recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    [self.timerLabel setText:@"0:00"];
    [_playButton setEnabled:YES]; //enable play button
    [self.navigationItem.rightBarButtonItem setEnabled:YES]; //enable Next button
   // success = YES;
    }
}

- (IBAction)playTaped:(id)sender {
    //playing record if it exists
    if (!recorder.recording){
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player play];
    }
}
//implementing protocol
- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                    message: @"You may tape Next or rewrite record"
                                                   delegate: nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}
//saving element and files
- (void) toSave
{
   
    success = YES;
    if (player.playing) {
        [player stop];
    }
    if (recorder.recording){
        [recorder stop];
    }
    ///close session
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Screen7 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen7"];
    [self.navigationController pushViewController:ctrl animated:YES];
   
    
}
@end
