//
//  DataManager.h
//  New
// Class for connection and managing CoreData
//  Created by User on 9/24/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "DataStorage.h"
#import "TourEntry.h"
@interface DataManager : NSObject{
 NSManagedObjectContext *context;

}
@property (weak, nonatomic) NSData* pngData;
@property (readonly) TourEntry *device;
//@property (nonatomic) NSString* imgpath;
//@property (nonatomic) NSString* audiopath;
+ (id)sharedManager;
-(void)createObject;
-(void) addItemToArray;
//get picture path from existing object
- (id)getPicturePath;
- (void)saveItem;
- (void) savePictureOnDisk;
- (NSMutableArray*) getAll; //get all database items
-(void) deleteObj: (NSManagedObject*) val; //delete object from database
-(void) unfinishedCreation; //rejects creation 
@end
