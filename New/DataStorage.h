//
//  DataStorage.h
//  New
//
//  Created by User on 9/28/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"
@interface DataStorage : NSObject
@property (strong, nonatomic) NSMutableArray* devices;

+ (DataStorage*)sharedStorage;
-(void) addItem: (NSManagedObject*) item;
@end
