//
//  Screen3.m
//  New
//
//  Created by User on 9/22/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Screen3.h"
#import "Screen4.h"
#import "Screen2.h"
#import <CoreData/CoreData.h>
@interface Screen3 (){
    NSManagedObjectContext *context;
    NSManagedObject *newDevice;
}

@end

@implementation Screen3

- (void)viewDidLoad {
    [super viewDidLoad];
    //create gradient
    CAGradientLayer* theViewGradient = [Tour createLayerBlueGradient];
    theViewGradient.frame = self.view.bounds;
    //Add gradient to view
    [self.view.layer insertSublayer:theViewGradient atIndex:0];

    [self.navigationController setNavigationBarHidden:true];
    
    
    
}
-(void) createMenu{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) //if camera exists
    {
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"
                                             destructiveButtonTitle:nil otherButtonTitles:
                                @"Library",
                                @"Camera",
                                nil];
        popup.tag = 1;
        [popup showInView:[UIApplication sharedApplication].keyWindow];
    }
    else{
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                @"Library",
                                nil,
                                nil];
        popup.tag = 1;
        [popup showInView:[UIApplication sharedApplication].keyWindow];
    }
}
//context

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self createMenu];
    [self.navigationController setNavigationBarHidden:true];
    //creating context
  DataManager  *dataManager = [DataManager sharedManager];
   // set latitude and longitude
    float lat =self.location.coordinate.latitude;
    float lon = self.location.coordinate.longitude;
   //initiating coordinates in single object
    dataManager.device.latitude = @(lat);
    dataManager.device.longitude = @(lon);
  

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//connect with action sheet
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
   
    if ([[popup buttonTitleAtIndex:buttonIndex] isEqual:@"Library"]) {
       [self clickLibrary];
    }
    if ([[popup buttonTitleAtIndex:buttonIndex] isEqual:@"Cancel"]) {
        [self clickCancel];
    }
    if ([[popup buttonTitleAtIndex:buttonIndex] isEqual:@"Camera"]) {
        [self clickCamera];
    }
}
- (void) clickCancel //cancel
{
    DataManager *dataManager = [DataManager sharedManager];
    [dataManager unfinishedCreation];
    [self.navigationController setNavigationBarHidden:false];
    [self.navigationController popViewControllerAnimated:YES];
 
}
- (void) clickCamera //with camera load flag galery = false
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Screen4 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen4"];
    ctrl.flag = false;
    
    [self.navigationController pushViewController:ctrl animated:YES];

}
- (void) clickLibrary //without camera
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Screen4 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen4"];
    ctrl.flag = YES;
    [self.navigationController pushViewController:ctrl animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
