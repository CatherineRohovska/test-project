//
//  ImageCell.m
//  New
//
//  Created by User on 9/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "ImageCell.h"

@implementation ImageCell
-(void) setImg: (NSString*) pathToImage
{
    self.photoImageView.image = [UIImage imageNamed: pathToImage];
}
@end
