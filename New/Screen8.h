//
//  Screen8.h
//  New
//
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "DataStorage.h"
#import "Tour.h"
#import "TourEntry.h"
#import "Screen9.h"
@interface Screen8 : UIViewController
<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tabView;
@property (strong, nonatomic) UITableView *tableView;
//@property (strong) NSMutableArray *devices;
@end
