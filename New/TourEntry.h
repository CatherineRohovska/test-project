//
//  TourEntry.h
//  New
//
//  Created by User on 9/28/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface TourEntry : NSManagedObject
@property(strong) NSString* name;
@property(strong) NSString* picpath;
@property(strong) NSString* audiopath;
@property(strong) NSNumber* latitude;
@property(strong) NSNumber* longitude;

@end
