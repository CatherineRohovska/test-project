//
//  Screen5.m
//  New
//
//  Created by User on 9/22/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Screen5.h"
#import "Screen6.h"

@interface Screen5 ()

@end

@implementation Screen5

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.txtName setDelegate:self];
    //create gradient
    CAGradientLayer* theViewGradient = [Tour createLayerBlueGradient];
    theViewGradient.frame = self.view.bounds;
    //Add gradient to view
    [self.view.layer insertSublayer:theViewGradient atIndex:0];
    
    //
    //adding button
    UIButton *anotherButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    anotherButton.frame = CGRectMake(0, 0, 80, 30);
    [anotherButton setTitle:@"Next" forState:UIControlStateNormal];
    [anotherButton addTarget:self action:@selector(toRecord:) forControlEvents:UIControlEventTouchUpInside];
    [anotherButton setBackgroundImage:[UIImage imageNamed:@"button_background.png"] forState:UIControlStateNormal];
    UIBarButtonItem* nextButton = [[UIBarButtonItem alloc] initWithCustomView:anotherButton];
    self.navigationItem.rightBarButtonItem = nextButton;
    
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //setting image
    self.imgView.image =[UIImage imageWithData:self.tmpImage];
  

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    
    if (theTextField == self.txtName) {
        
        [theTextField resignFirstResponder];
        
    }
    
    return YES;
    
}

//to record view
- (IBAction)toRecord:(id)sender{
    if (![self.txtName.text isEqualToString:(@"")]) //if textfield is not empty
    {
        
        DataManager* dataManager = [DataManager sharedManager];

        //adding core data
        dataManager.device.name =self.txtName.text;
        dataManager.device.picpath =[[self.txtName.text stringByAppendingString:[Tour timeStamp]]stringByAppendingString:@".png"];
        dataManager.pngData =self.tmpImage;
        //
       
    
        //end core data
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Screen6 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen6"];
        ctrl.name = self.txtName.text;
    [self.navigationController pushViewController:ctrl animated:YES];
    }
}
//allow numbers, digits and _
- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    //return yes or no after comparing the characters
    // allow backspace
    if (!string.length)
    {
        return YES;
    }
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_"] invertedSet];
    
    if ([string rangeOfCharacterFromSet:set].location == NSNotFound) {
        return YES;
    }
   
    
    return NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
