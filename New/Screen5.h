//
//  Screen5.h
//  New
//
//  Created by User on 9/22/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "Tour.h"
@interface Screen5 : UIViewController
<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (nonatomic) NSData *tmpImage;

@end
