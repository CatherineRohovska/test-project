//
//  Screen2.m
//  New
//
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Screen2.h"

#import "Screen3.h"
@interface Screen2 ()
{
    CLLocationManager *locationManager;
}

@end

@implementation Screen2

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:false];
    
   //create gradient
    CAGradientLayer* theViewGradient = [Tour createLayerBlueGradient];
    theViewGradient.frame = self.view.bounds;
    //Add gradient to view
    [self.view.layer insertSublayer:theViewGradient atIndex:0];
    
    // create mapview
    [self.view addSubview:self.mapView]; //adding subview to main view
    self.mapView.delegate=self;
    //show user location
    self.mapView.showsUserLocation = YES;
    locationManager = [[CLLocationManager alloc] init];
    [locationManager startUpdatingLocation];
    //cutomizing button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"New" forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"button_background.png"] forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0.0f, 0.0f, 80.0f, 30.0f)];
    [btn addTarget:self action:@selector(openNewPic:) forControlEvents:UIControlEventTouchUpInside];
    //adding button to Navigator
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
  //  anotherButton.image = [UIImage imageNamed:@"button_background.png"];
    self.navigationItem.rightBarButtonItem = anotherButton;
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSMutableArray *items;
    
    DataManager* dataManager = [DataManager sharedManager];
    DataStorage* dataStorage = [DataStorage sharedStorage];
    items = [dataManager getAll];
    TourEntry *device;
//creation of set of marks
    for (int i=0; i< dataStorage.devices.count; i++)
    {
        device = [dataStorage.devices objectAtIndex:i];
        CLLocationCoordinate2D mycoord = CLLocationCoordinate2DMake((CLLocationDegrees)[device.latitude doubleValue], (CLLocationDegrees)[device.longitude doubleValue]);
         //get path to picture
        NSString *workSpacePath=[[Tour applicationDocumentsDirectory] stringByAppendingPathComponent:device.picpath];
        MyAnnotation *zp = [[MyAnnotation alloc] initWithTitle:device.name Location: mycoord Img:workSpacePath index:i];
        zp.delegate = self;
        [self.mapView addAnnotation:zp];
    }
}
//
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocationCoordinate2D userCoordinate = manager.location.coordinate;
    [self.mapView setCenterCoordinate:userCoordinate animated:YES];
    [self.mapView setShowsUserLocation:YES];
}
//
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    [self.mapView setShowsUserLocation:YES];
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    //center map on current location
    [self.mapView setCenterCoordinate:self.mapView.userLocation.location.coordinate animated:YES];
}

//annotation place
-(MKAnnotationView*) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
   
    
    if([annotation isKindOfClass:[MyAnnotation class]])
    {
        MyAnnotation *myLocation = (MyAnnotation*) annotation;
        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"Custom"];
        
//        if (annotationView == nil)
            annotationView = myLocation.annotationView;
//        else
//            annotationView.annotation = annotation;
        return annotationView;
    }
    else
    {
       return nil;
    }
    
    
}
//location


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//open selection image menu
- (IBAction)openNewPic:(id)sender {
    if(self.mapView.userLocation.coordinate.latitude!=0.0){
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Screen3 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen3"];
    ctrl.location = self.mapView.userLocation;
    NSLog(@"%f",self.mapView.userLocation.coordinate.latitude);
    
    DataManager *dataManager = [DataManager sharedManager];
    [dataManager createObject];
    [self.navigationController pushViewController:ctrl animated:YES];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Coordinates are unavaliable"
                                                        message: @"Please, wait till updating"
                                                       delegate: self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//set type of map
- (IBAction)mapType:(id)sender {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:
                            @"Standart",
                            @"Satellite",
                            @"Hybrid",
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    //[self.mapView setMapType:MKMapTypeSatellite];
}
//bind actions to buttons on menu
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
switch (popup.tag) {
    case 1: {
     switch (buttonIndex) {
         case 0:
             [self.mapView setMapType:MKMapTypeStandard];
             break;
         case 1:
             [self.mapView setMapType:MKMapTypeSatellite];
             break;
         case 2:
             [self.mapView setMapType:MKMapTypeHybrid];
             break;
         default:
             break;
     }
        break;
     }
     default:
     break;
     }
     }
//-(void) setMaps: (UIButton*) sender
//{
//   // NSLog(@"Button clicked");
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    Screen9 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen9"];
//    ctrl.bookmark = (int)sender.tag;
//   // NSLog(@"%ld",(long)sender.tag);
//    [self.navigationController pushViewController:ctrl animated:YES];
//    
//}
//delegate realization
-(void)infoButtonClick: (int) index{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Screen9 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen9"];
    ctrl.bookmark = index;
    // NSLog(@"%ld",(long)sender.tag);
    [self.navigationController pushViewController:ctrl animated:YES];
}
-(void) dealloc{
    NSLog(@"deallocate memory");
    self.mapView.delegate = nil;
    self.mapView = nil;
    [locationManager stopUpdatingLocation];
    locationManager = nil;
}
@end
