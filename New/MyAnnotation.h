//
//  MyAnnotation.h
//  New
// Create custom annotation
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@protocol MyAnnotationDelegate<NSObject>
@required
- (void)infoButtonClick: (int) index;
@end
@interface MyAnnotation : NSObject <MKAnnotation>
{
//id <MyAnnotationDelegate> delegate;
}
@property (weak,nonatomic)id <MyAnnotationDelegate> delegate;
@property (nonatomic, readonly) CLLocationCoordinate2D  coordinate; //store coordinates
@property (copy, nonatomic) NSString *title;
@property (nonatomic) NSString *imgPath;
@property (nonatomic) SEL act;
@property (weak, nonatomic) id targ;
@property (nonatomic) int indexCatalog;
- (id)initWithTitle:(NSString *) newTitle Location: (CLLocationCoordinate2D) location Img: (NSString*) str index: (int) mark;
- (MKAnnotationView*) annotationView;
//delegation perform
-(void) onClick;
@end
