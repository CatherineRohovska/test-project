//
//  DataManager.m
//  New
//
//  Created by User on 9/24/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager
@synthesize pngData = _pngData;
//@synthesize imgpath = _imgpath;
+ (id)sharedManager {
    static DataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *ncontext = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        ncontext = [delegate managedObjectContext];
    }
    return ncontext;
}
- (id)init {
    if (self = [super init]) {
        
        //initiate
      context = [self managedObjectContext];
       
    }
    return self;
}
-(void) createObject{
    // Create a new managed object
    _device = [NSEntityDescription insertNewObjectForEntityForName:@"Tour" inManagedObjectContext:context];
}
//gets
- (id)getPicturePath
{
    return [_device valueForKey:@"picpath"];
}
-(void) saveItem
{
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error])
    {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
 
}
-(void) savePictureOnDisk{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    [self.pngData writeToFile:[documentsPath stringByAppendingPathComponent:[self getPicturePath]] atomically:YES];
}
-(NSMutableArray*) getAll{
    context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Tour"];
    return [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
}
-(void) deleteObj: (NSManagedObject*) val{
    [context deleteObject:val];
    [self saveItem];
   // device = [NSEntityDescription insertNewObjectForEntityForName:@"Tour" inManagedObjectContext:context];
}
-(void) unfinishedCreation
{
    [context deleteObject:_device];
}
-(void) addItemToArray{
    DataStorage* dataStorage = [DataStorage sharedStorage];
    [dataStorage addItem:(_device)];
}
- (void)dealloc {
    // Should never be called
}
@end
