//
//  Screen3.h
//  New
//
//  Created by User on 9/22/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DataManager.h"
@interface Screen3 : UIViewController
<UIActionSheetDelegate>
@property (nonatomic) MKUserLocation *location;
//@property (nonatomic) NSManagedObjectContext *context;
@end
