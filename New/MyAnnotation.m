//
//  MyAnnotation.m
//  New
//
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "MyAnnotation.h"
#import "Screen2.h"
@implementation MyAnnotation

- (id)initWithTitle:(NSString *) newTitle Location: (CLLocationCoordinate2D) location Img: (NSString*) str index: (int) mark {
    self = [super init];
    
    if (self)
    {
        _title = newTitle;
        _coordinate = location;
        _imgPath = str;
        _indexCatalog = mark;
    }
    return self;
    
}
- (MKAnnotationView*) annotationView
{
    MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"Custom"];
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    //add image and button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    CGRect frame = CGRectMake(0, 0, 15, 15);
    button.frame = frame;
    button.tag = _indexCatalog;
    [button addTarget:self
    action: @selector(onClick)
    forControlEvents:UIControlEventTouchUpInside];
    annotationView.rightCalloutAccessoryView = button;
   UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
   imageView.image = [UIImage imageNamed:self.imgPath];
    annotationView.leftCalloutAccessoryView = imageView;

    return annotationView;
    
}
-(void) onClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(infoButtonClick:)])
    {
        [self.delegate infoButtonClick:_indexCatalog];
    }
    
}


@end
