//
//  Screen9.m
//  New
//
//  Created by User on 9/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Screen9.h"

@interface Screen9 ()
{
    bool initialScrollDone;
    AVAudioPlayer *player;
    //timer
    NSTimer *timer;
    int seconds;
    NSTimeInterval durationInSeconds; //duration of audio record
    NSTimeInterval currentTimeInSeconds; //current time position
 
    //scrolling
    NSMutableArray* heights;
    NSMutableArray* widths;
    double scrollX;
    int currentPage;
}

@end

@implementation Screen9

- (void)viewDidLoad {
    [super viewDidLoad];
    //create gradient
    CAGradientLayer* theViewGradient = [Tour createLayerBlueGradient];
    theViewGradient.frame = self.view.bounds;
    //Add gradient to view
    [self.view.layer insertSublayer:theViewGradient atIndex:0];
    
    // Do any additional setup after loading the view.
    initialScrollDone = NO;
    //while player not load
    [self.playerProgressBar addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    heights = [NSMutableArray array];
    widths = [NSMutableArray array];
    UIImage* tmp;
    DataStorage* dataStorage = [DataStorage sharedStorage];
    TourEntry *device;
    //
    double height = self.view.bounds.size.height;
  //counting width and heights
    for (int i=0; i< dataStorage.devices.count; i++)
    {
        device = [dataStorage.devices objectAtIndex:i]; //get namaged object
        NSString *workSpacePath=[[Tour applicationDocumentsDirectory] stringByAppendingPathComponent:device.picpath]; //build directory path

        tmp = [UIImage imageNamed:workSpacePath]; //get image
        [heights addObject:[NSNumber numberWithFloat:tmp.size.height]]; //adding height to array
        [widths addObject:[NSNumber numberWithFloat:(tmp.size.width)*(height/tmp.size.height)]]; //calculating and adding width to array

    }
    //first definition of player
    device  = [dataStorage.devices objectAtIndex:self.bookmark];
    [self.navigationItem setTitle:device.name]; //set title to bar
    //setting audio to player
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [Tour applicationDocumentsDirectory],
                               device.audiopath,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:outputFileURL error:nil];
    [player setDelegate:self];
    
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.imgColletion reloadData];
    //set player, timer and progress bar
    [self.playerTimeLabel setText:@"0:00"];
    seconds = 0;
    [self.playerProgressBar setValue:0 animated:NO];
     
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil]; //define behaviour to play audio file

   
    
}

- (void)viewDidLayoutSubviews {
    
    // If haven't done the initial scroll, do it once.
    if (!initialScrollDone) {
        initialScrollDone = YES;
        
        [self.imgColletion scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.bookmark inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(player.playing){
        [player stop]; //stop player if it is playing
    }
    [timer invalidate]; //stop timer
    //close audio session
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//acting with Collection

//when scroll is happened
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{  //current page
    scrollX = self.imgColletion.contentOffset.x+self.view.bounds.size.width/2; //position on screen
    double scrolling = 0;
    int prev_page = currentPage;
    for (int i=0; i< widths.count; i++)
    {
        scrolling = scrolling+[widths[i] doubleValue];
        if(scrollX<scrolling) {currentPage = i; break;}
    }
   // NSLog(@"%d",currentPage);
    //reload data if page is changing
    if (prev_page!=currentPage){
        //stop player, stop timer and play-button title
        [self.playerTimeLabel setText:@"0:00"];
        [timer invalidate];
        seconds = 0;
        [self.playPauseButton setTitle:@"Play" forState:UIControlStateNormal];
        [self.playerProgressBar setEnabled:YES];
        [self.playerProgressBar setValue:0 animated:NO];
        //create new entry
     DataStorage* dataStorage = [DataStorage sharedStorage];
     TourEntry *device = [dataStorage.devices objectAtIndex:currentPage];
    [self.navigationItem setTitle:device.name]; //set title to bar
    //setting audio to player
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [Tour applicationDocumentsDirectory],
                               device.audiopath,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:outputFileURL error:nil];
    [player setDelegate:self];
    //get audio duration
    durationInSeconds = player.duration;
    [self.playerProgressBar setMaximumValue:lroundf(durationInSeconds) ];
       
    }
     //
}
//count of cells;
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    DataStorage* dataStorage = [DataStorage sharedStorage];
    
    return dataStorage.devices.count;
}
//creating visible cell
- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //create cell
    ImageCell *cell = (ImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ImgCell" forIndexPath:indexPath];
   DataStorage* dataStorage = [DataStorage sharedStorage];
    //adding image
   TourEntry *device = [dataStorage.devices objectAtIndex:indexPath.row];
    NSString *workSpacePath=[[Tour applicationDocumentsDirectory] stringByAppendingPathComponent:device.picpath];
      [cell setImg:workSpacePath]; //set image to cell
        return cell;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}
//size of image cell
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    double height = self.imgColletion.bounds.size.height;//self.view.bounds.size.height;
    CGSize size;
    size.width = [[widths objectAtIndex:indexPath.item] doubleValue];
    size.height = height;
    return size;
}
//dragging
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    
   // NSLog(@"%f",targetContentOffset->x);
    double contentCounter = 0;
    for(int i=0; i<widths.count; i++)
    {
        contentCounter = contentCounter+[widths[i] doubleValue]; //border position between two pictures
        //if border between half and end of the screen;
        if((targetContentOffset->x + self.view.bounds.size.width/2 < contentCounter)&(targetContentOffset->x + self.view.bounds.size.width> contentCounter))
        {
            targetContentOffset->x =contentCounter-self.view.bounds.size.width;
            break;
        }
        //if border between begining and half of the screen  and next picture exists
        if((targetContentOffset->x + self.view.bounds.size.width/2 > contentCounter)&(contentCounter>targetContentOffset->x)&(i+1<widths.count))
        {
            //if velocity vector is positive
            if (velocity.x>0){
                //go to the next picture
                targetContentOffset->x = contentCounter+[widths[i+1] doubleValue]/2-self.view.bounds.size.width/2;
            }
            else{
                //else go to the border
                targetContentOffset->x = contentCounter;
            }
           //contentCounter = contentCounter+[widths[i] doubleValue];

            break;
        }
    }

}
/////////////////////////////
//on timer tick
- (void)updateTimer:(NSTimer *)theTimer {
    float duration = durationInSeconds;
    if (duration>=0) {
        if(seconds==0){
            [self.playerTimeLabel setText:@"0:00"];}
        seconds++;
        duration--;
        int min = seconds/60;
        int sec = seconds%60;
        [self.playerTimeLabel setText:[NSString stringWithFormat:@"%01d:%02d",min,sec]];
        
        [self.playerProgressBar setValue:player.currentTime animated:NO];
       
              
    } else {
        [timer invalidate];
        [self.playerTimeLabel setText:@"0:00"];
        NSLog(@"Timer end");
    }
    
    
    
}
//click on Play\Pause
- (IBAction)playPauseButton:(id)sender {
    // Stop the audio player if it is playing
    if (player.playing) {
        [self.playPauseButton setTitle:@"Play" forState:UIControlStateNormal];
        [player pause];
        [timer invalidate];
    }
    else{
        [self.playPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        [player play];
         timer = [NSTimer scheduledTimerWithTimeInterval:1.0f  target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
        [self.playerProgressBar setEnabled:YES];
    }
}
//when finished to play
- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    seconds = 0;
    [self.playerTimeLabel setText:@"0:00"];
    [timer invalidate];
    [self.playPauseButton setTitle:@"Play" forState:UIControlStateNormal];
   // [self.playerProgressBar setEnabled:NO];
    [self.playerProgressBar setValue:0 animated:NO];
}
// slider value changed
- (IBAction)sliderValueChanged:(UISlider *)sender {
   // NSLog(@"slider value = %f", sender.value);
    currentTimeInSeconds = sender.value;
    player.currentTime = self.playerProgressBar.value;
    seconds = currentTimeInSeconds;
    int min = seconds/60;
    int sec = seconds%60;
    [self.playerTimeLabel setText:[NSString stringWithFormat:@"%01d:%02d",min,sec]];

}
-(void) dealloc{
    NSLog(@"dealloc detected");
}
@end
