//
//  Tour.h
//  New
//
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>
@interface Tour : NSObject{
    
    
}
+(NSString *)applicationDocumentsDirectory;
+(NSString *) timeStamp;
+(CAGradientLayer *) createLayerBlueGradient;
@end
