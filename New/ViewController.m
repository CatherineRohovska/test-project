//
//  ViewController.m
//  New
//
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import "Screen8.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //creating location manager
    CLLocationManager *loc = [CLLocationManager new];
    //sending request
    [loc requestWhenInUseAuthorization];
//self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_gradient.jpg"]];
    // Do any additional setup after loading the view, typically from a nib.
   
    CAGradientLayer* theViewGradient = [Tour createLayerBlueGradient];
    theViewGradient.frame = self.view.bounds;
    //Add gradient to view
    [self.view.layer insertSublayer:theViewGradient atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
//     Dispose of any resources that can be recreated
}




//cathching open new view event

- (IBAction)openMapView:(id)sender {
    
}

- (IBAction)openTourList:(id)sender {
    Screen8* tourList = [[Screen8 alloc] init];
    [self.navigationController pushViewController:tourList animated:YES];
   // [tourList release];
}
@end
