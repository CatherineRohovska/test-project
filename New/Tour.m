//
//  Tour.m
//  New
//
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Tour.h"
#import <UIKit/UIKit.h>
@implementation Tour
+ (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}
+ (NSString *) timeStamp {
    return [NSString stringWithFormat:@"%i",(int)ceil([[NSDate date] timeIntervalSince1970] * 1000)];
}
+ (CAGradientLayer *)createLayerBlueGradient
{
    // Create the colors
    UIColor *topColor = [UIColor colorWithRed:3.0/255.0 green:28.0/255.0 blue:129.0/255.0 alpha:1.0];
    UIColor *bottomColor = [UIColor colorWithRed:3.0/255.0 green:70.0/255.0 blue:129.0/255.0 alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.colors = [NSArray arrayWithObjects: (id)topColor.CGColor, (id)bottomColor.CGColor, nil];
    //theViewGradient.frame = self.view.bounds;
    return theViewGradient;
}

@end
