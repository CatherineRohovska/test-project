//
//  Screen4.m
//  New
//
//  Created by User on 9/22/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Screen4.h"
#import "Screen3.h"
#import "Screen5.h"
@interface Screen4 ()
@end

@implementation Screen4
//@synthesize imView  = _imView;
- (void)viewDidLoad {
    [super viewDidLoad];
     [self.navigationController setNavigationBarHidden:false];
    // Do any additional setup after loading the view.
    
    //create gradient
    CAGradientLayer* theViewGradient = [Tour createLayerBlueGradient];
    theViewGradient.frame = self.view.bounds;
    //Add gradient to view
    [self.view.layer insertSublayer:theViewGradient atIndex:0];
    
    //adding button
    UIButton *anotherButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    anotherButton.frame = CGRectMake(0, 0, 80, 30);
    [anotherButton setTitle:@"Next" forState:UIControlStateNormal];
    [anotherButton addTarget:self action:@selector(takePicture:) forControlEvents:UIControlEventTouchUpInside];
    [anotherButton setBackgroundImage:[UIImage imageNamed:@"button_background.png"] forState:UIControlStateNormal];
    UIBarButtonItem* nextButton = [[UIBarButtonItem alloc] initWithCustomView:anotherButton];
//    [anotherButton setBackButtonBackgroundImage:[UIImage imageNamed:@"button_background.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    self.navigationItem.rightBarButtonItem = nextButton;
  
   // self.flag = true; load from gallery
    if (self.flag)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    else { //self.flag = false; load from camera
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//to next view with picture name
- (IBAction)takePicture:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Screen5 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen5"];
     //setting image
    ctrl.tmpImage = UIImagePNGRepresentation(self.imView.image);
  
 
    [self.navigationController pushViewController:ctrl animated:YES];
}
//reload methods from imagePickerController
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    Screen3 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen3"];
//    //ctrl.flag = YES;
//    [self.navigationController pushViewController:ctrl animated:YES];
    [self.navigationController setNavigationBarHidden:true];
    [self.navigationController popViewControllerAnimated:YES];


    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
