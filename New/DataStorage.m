//
//  DataStorage.m
//  New
//
//  Created by User on 9/28/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "DataStorage.h"

@implementation DataStorage
+ (id)sharedStorage {
    static DataStorage *sharedThisStorage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedThisStorage = [[self alloc] init];
    });
    return sharedThisStorage;
}
- (id)init {
    if (self = [super init]) {
        
        //initiate
        DataManager* dataManager = [DataManager sharedManager];
        self.devices = [dataManager getAll];
        
    }
    return self;
}
//adding new device to array
-(void) addItem:(NSManagedObject *)item{
    [self.devices addObject:item];
}
@end
