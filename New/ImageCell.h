//
//  ImageCell.h
//  New
//
//  Created by User on 9/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCell : UICollectionViewCell
@property(nonatomic, weak) IBOutlet UIImageView *photoImageView;
-(void) setImg: (NSString*) pathToImage;
@end
