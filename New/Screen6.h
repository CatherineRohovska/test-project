//
//  Screen6.h
//  New
// From player
//  Created by User on 9/22/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "DataManager.h"
#import "Screen7.h"
#import "Tour.h"
@interface Screen6 : UIViewController
<AVAudioRecorderDelegate, AVAudioPlayerDelegate>
@property (nonatomic) NSString *name;
@property (strong, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *levelMeter;
- (IBAction)recordPauseTaped:(id)sender;
- (IBAction)stopTaped:(id)sender;
- (IBAction)playTaped:(id)sender;

@end
