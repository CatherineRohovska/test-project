//
//  Screen7.m
//  New
//
//  Created by User on 9/24/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Screen7.h"

@interface Screen7 ()

@end

@implementation Screen7

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //create gradient
    CAGradientLayer* theViewGradient = [Tour createLayerBlueGradient];
    theViewGradient.frame = self.view.bounds;
    //Add gradient to view
    [self.view.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.navigationController setToolbarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                    message: @"Your tour is saved!"
                                                   delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==0) {
           // Do something for button #1
        //save picture and database row on disk
            DataManager* dataManager = [DataManager sharedManager];
            [dataManager saveItem]; //save item in the context
            [dataManager addItemToArray]; //add new item to array of tours
            [dataManager savePictureOnDisk]; //save picture on disk

        //return to screen 2 with map
        UIViewController *prevVC = [self.navigationController.viewControllers objectAtIndex:1];
        [self.navigationController popToViewController:prevVC animated:YES];
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
