//
//  ViewController.h
//  New
//
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tour.h"
@interface ViewController : UIViewController
<UITextFieldDelegate>
- (IBAction)openMapView:(id)sender;
- (IBAction)openTourList:(id)sender;


@end

