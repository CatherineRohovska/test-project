//
//  Screen9.h
//  New
//
//  Created by User on 9/25/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ImageCell.h"
#import "DataManager.h"
#import <AVFoundation/AVPlayer.h>
#import <AVFoundation/AVPlayerItem.h>
#import <AVFoundation/AVAsset.h>
#include "Tour.h"
@interface Screen9 : UIViewController
<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *imgColletion;
@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;

@property (weak, nonatomic) IBOutlet UISlider *playerProgressBar;
@property (weak, nonatomic) IBOutlet UILabel *playerTimeLabel;
@property(nonatomic) int bookmark; //to scroll image collection
- (IBAction)playPauseButton:(id)sender;
@end
