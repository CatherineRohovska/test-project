//
//  Screen2.h
//  New
//
//  Created by User on 9/21/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DataManager.h"
#import "Tour.h"
#import "Screen9.h"
#import "MyAnnotation.h"
@interface Screen2 : UIViewController
<MKMapViewDelegate, UIActionSheetDelegate, MyAnnotationDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)mapType:(id)sender;
@end
